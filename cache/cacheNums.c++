#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string

using namespace std;

int main() {
  string s;
  while (getline(cin, s)) {
    istringstream sin(s);
    int i;
    int j;
    int n;
    sin >> i >> j >> n;
    cout << n << ", ";
  }
  cout << endl;
}
