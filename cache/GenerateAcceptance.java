/* A class to generate random acceptance tests for the max
   Collatz Conjecture cycle length in a range */

public class GenerateAcceptance {

    // Creates NUM_TESTS random tests
    public static void main(String[] args) {
        final int NUM_TESTS = 300;
	for (int i = 0; i < NUM_TESTS; i++) {
	    int num1 = getRandNum();
            int num2 = getRandNum();
	    System.out.println(num1 + " " + num2);
        }
    }

    // Returns a random int between 1 and 1000000
    public static int getRandNum() {
	return (int) (Math.random() * 1000000) + 1;
    }

}
