#include <iostream> // cin, cout
#include <sstream>  // istringstream
#include <string>   // getline, string

using namespace std;

int main () {
    int cache[1000];
    string s;
    int i = 0;
    while (getline(cin, s)) {
        istringstream sin(s);
        int i; int j; int num;
        sin >> i >> j;
        sin >> num;
        cache[i] = num;
        i++;
    }
    for (int i = 0; i < 1000; ++i) {
        cout << cache[i] << endl;
    }
}
